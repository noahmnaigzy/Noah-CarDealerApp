import React, { useEffect, useState } from 'react'

function SalesHistory() {
    const [salespeople, SetSalespeople] = useState([]);
    const [sales, SetSales] = useState([]);
    const [salesperson, setSalesPerson] = useState('');

    async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            SetSalespeople(data.salespeople)
        } else {
            console.error(response)
        }
    }

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            SetSales(data.sales)
        } else {
            console.error(response)
        }
    }

useEffect(() => {
    loadSalespeople()
    loadSales()
}, [])

function handleSalesPersonChange(event) {
    const salesPersonId = event.target.value;
    setSalesPerson(salesPersonId);
}

const filterSales = sales.filter(sale => sale.sales_person.id.toString() === salesperson)

return (
<div>
  <h1>Salesperson History</h1>
  <select value={salesperson} onChange={handleSalesPersonChange} required name="salesperson" id="salesperson" className="form-select">
        <option value="">Choose a Customer</option>
        {salespeople.map(salesperson => {
    return (
    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
) })}
    </select>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {filterSales.map(sale => {
          return (
            <tr key={sale.id}>
              <td>{ sale.sales_person.first_name} { sale.sales_person.last_name} </td>
              <td>{ sale.customer.first_name} { sale.customer.last_name}</td>
              <td>{ sale.automobile.vin}</td>
              <td>{ sale.price}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
</div>
  );

}

export default SalesHistory
