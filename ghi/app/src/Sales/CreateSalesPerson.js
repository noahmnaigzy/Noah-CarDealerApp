import React, {useEffect, useState } from 'react';


function CreateSalesPerson() {
    const [ firstname, setFirstname] = useState('');
    const [ lastname, setLastName] = useState('');
    const [ employeeId, setEmployeeId] = useState('');
    const [ salespeople, setSalesPeople] = useState([]);
    const [confirmation, setConfirmation] = useState(false)

    async function loadSalesPeople() {
        const url = 'http://localhost:8090/api/salespeople/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setSalesPeople(data.salespeople);
        }
      }
      useEffect(() => {
        loadSalesPeople();
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        setConfirmation(false)
        const data = {
          first_name: firstname,
          last_name:lastname,
          employee_id:employeeId,



        };



    const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      setConfirmation(true)
      setFirstname('');
      setLastName('');
      setEmployeeId('');
      loadSalesPeople();

    }
  }

  function handleChangeFirstName(event) {
    const { value } = event.target;
    setFirstname(value);
  }

  function handleChangeLastName(event) {
    const { value } = event.target;
    setLastName(value);
  }

  function handleChangeEmployeeId(event) {
    const { value } = event.target;
    setEmployeeId(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          {confirmation && <div className="alert alert-success">Salesperson Created!</div>}
          <form onSubmit={handleSubmit} id="create-salesPerson-form">
            <div className="form-floating mb-3">
              <input value={firstname} onChange={handleChangeFirstName} placeholder="First Name" required type="text" name="first name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={lastname} onChange={handleChangeLastName} placeholder="Last Name" required type="text" name="last name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employeeId} onChange={handleChangeEmployeeId} placeholder="Employee ID" required type="text" name="employee id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee ID</label>
            </div>

            <button className="btn btn-primary">Create Sales person</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateSalesPerson;
