import React, {useEffect, useState } from 'react';


function CreateSale() {
    const [ price, setPrice] = useState('');
    const [ automobile, setAutomobile] = useState('');
    const [ salesperson, setSalesPerson] = useState('');
    const [ customer, setCustomer] = useState('');
    const [ customers, setCustomers] = useState([]);
    const [ salespeople, setSalesPeople] = useState([]);
    const [ autos, setAutos] = useState([]);
    const [confirmation, setConfirmation] = useState(false)

    async function loadCustomers() {
      const url = 'http://localhost:8090/api/customers/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      }
    }
    async function loadSalesPeople() {
      const url = 'http://localhost:8090/api/salespeople/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.salespeople);
      }
    }

      async function loadAutos() {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          const available = data.autos.filter(auto => !auto.sold);
          setAutos(available);
        }
      }
      useEffect(() => {
        loadSalesPeople();
        loadAutos();
        loadCustomers()
      }, [])

    async function handleSubmit(event) {
        event.preventDefault();
        setConfirmation(false)
        const data = {
          price: price,
          automobile:automobile,
          sales_person:salesperson,
          customer:customer,
        };



    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      setConfirmation(true)
      setPrice('');
      setAutomobile('');
      setSalesPerson('');
      setCustomer('')

      const soldUpdateUrl = `http://localhost:8100/api/automobiles/${automobile}/`
      const update = {
        "sold": true,
      }
      const putConfig = {
        method: 'PUT',
        body: JSON.stringify(update),
        headers: {
          'Content-Type': 'application/json'
        }
      }
      const soldResponse = await fetch(soldUpdateUrl, putConfig)
      if (soldResponse.ok) {
        console.log("Auto Sold updated")
        loadAutos()
      } else{
        console.error("failed to update auto sold attribute")
      }
    } else {
      console.error("sale creation failed ")
    }
  }

  function handleChangePrice(event) {
    const { value } = event.target;
    setPrice(value);
  }

  function handleChangeAuto(event) {
    const { value } = event.target;
    setAutomobile(value);
  }

  function handleChangeSalesPerson(event) {
    const { value } = event.target;
    setSalesPerson(value);
  }
  function handleChangeCustomer(event) {
    const { value } = event.target;
    setCustomer(value);
  }




  return (
    <div className="my-5 container">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a sale</h1>
          {confirmation && <div className="alert alert-success">Sale Created!</div>}
          <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="mb-3">
                  <label>Automobile Vin</label>
                  <div className="mb-1"></div>
                    <select value={automobile} onChange={handleChangeAuto} required name="Auto" id="Auto" className="form-select">
                      <option value="">Choose an Automobile VIN</option>
                      {autos.map(auto => {
                        return (
                          <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                        )
                      })}
                    </select>
                </div>
                <div className="mb-3">
                <label>Salesperson</label>
                <div className="mb-1"></div>
                    <select value={salesperson} onChange={handleChangeSalesPerson} required name="sales_person" id="sales_person" className="form-select">
                      <option value="">Choose a Salesperson</option>
                      {salespeople.map(salesperson => {
                        return (
                          <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                        )
                      })}
                    </select>
                </div>
                <div className="mb-3">
                <label>Customer</label>
                <div className="mb-1"></div>
                    <select value={customer} onChange={handleChangeCustomer} required name="customer" id="customer" className="form-select">
                      <option value="">Choose a Customer</option>
                      {customers.map(custom => {
                        return (
                          <option key={custom.id} value={custom.id}>{custom.first_name} {custom.last_name}</option>
                        )
                      })}
                    </select>
                </div>
                <div className="form-floating mb-3">

                    <input value={price} onChange={handleChangePrice} placeholder="price" required type="text" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Price </label>
                </div>
                <button className="btn btn-primary">Create a Sale</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default CreateSale;
