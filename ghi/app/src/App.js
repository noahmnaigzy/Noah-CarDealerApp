import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListVehicles from "./ListVehicle";
import ListAutomobiles from "./ListAutos";
import CreateVehicleModel from "./CreateVehicle";
import AddTech from "./AddTech";
import TechnicianList from "./ListTech.js";
import ServiceAppointment from "./ServiceAppointmentForm";
import ApptsList from "./ListAppt";
import ServiceHistory from "./ServiceHistory";
import Listmanufacturer from "./Listmanufacturer";
import AddManufacturer from "./AddManufacturer";
import AddAuto from "./AddAuto";
import React, { useEffect, useState } from "react";

import ListSales from "./Sales/ListSales";
import ListCustomers from "./Sales/ListCustomers";
import ListSalespeople from "./Sales/ListSalespeople";
import CustomerForm from "./Sales/CreateCustomer";
import CreateSalesPerson from "./Sales/CreateSalesPerson";
import CreateSale from "./Sales/CreateSale";
import SalesHistory from "./Sales/SalesPersonHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/models" element={<ListVehicles />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/models/create" element={<CreateVehicleModel />} />
          <Route path="/technicians/create" element={<AddTech />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/appointments/create" element={<ServiceAppointment />} />
          <Route path="/appointments" element={<ApptsList />} />
          <Route path="/appointments/history" element={<ServiceHistory />} />
          <Route path="/manufacturers" element={<Listmanufacturer />} />
          <Route path="/manufacturers/create" element={<AddManufacturer />} />
          <Route path="/automobiles/create" element={<AddAuto />} />
          <Route path="/sales" element={<ListSales />} />
          <Route path="/sales/new" element={<CreateSale />} />
          <Route path="/customers" element={<ListCustomers />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/salespeople" element={<ListSalespeople />} />
          <Route path="/salespeople/new" element={<CreateSalesPerson />} />
          <Route path="/salespeople/history" element={<SalesHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
