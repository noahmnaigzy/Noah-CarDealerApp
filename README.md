# CarCar
CarCar is a dealership management application. Created to meet the everyday demands of car dealers, this application
simplifies complex tasks, offering an easy to use interface and automated processes. CarCar helps manage vehicle inventories,
customer relations, sales monitoring, and post-sale services.
Team:

* Juan - Auto Sales
* Noah - Auto Service

## Getting Started

1. **Fork this repository**.

2. **Clone the forked repository onto your local computer**:

   - Command:
     ```shell
     git clone https://gitlab.com/jmelendrez5640/Car-dealer-application
     ```

3. **Build and run the project using Docker** with these commands:

   - Create a Docker volume:
     ```shell
     docker volume create beta-data
     ```

   - Build the project:
     ```shell
     docker-compose build
     ```

   - Start the containers:
     ```shell
     docker-compose up
     ```

   After running these commands, you should have all Docker containers running.

4. **Access the project** at the following URL:

   - [http://localhost:3000/](http://localhost:3000/)


## Inventory Service:

- Manufacturer Model
    - Property: name
        - Stores information about different vehicle manufacturers available in the inventory.

- VehicleModel Model
    - Properties: name, picture_url, manufacturer
        - name holds the name of the vehicle model.
        - picture_url accepts a picture URL for reference.
        - manufacturer is a foreign key, referencing the manufacturer's name from the Manufacturer Model.
- Automobile Model
    - Properties: color, vin, year, model
        - color stores the color of the vehicle.
        - vin holds the unique VIN (Vehicle Identification Number) of the vehicle.
        - year captures the manufacturing year of the vehicle.
        - model is a foreign key, referring to the VehicleModel for the name of the model.

- The data in the Automobile Model is frequently accessed by other microservices through a poller to ensure up-to-date inventory information, enhancing the customer experience.


## Service microservice

Key Functionalities:

- Technicians:

    - Create: 
    This functionality allows the system to add new technicians to the service team.
    - Delete: 
    Technicians can be removed from the system; this action is not directly available through the website but is accessed through Insomnia for administrative purposes.

- Appointments:

    - Create: The microservice provides the capability to schedule service appointments for customers and their vehicles.
    - Update: Appointments can be modified as needed to accommodate changes in scheduling or service requirements.
    - Delete: This functionality allows the cancellation and removal of appointments.
    - Service History:
    The Service Microservice maintains a log of all appointments made. This history is for tracking and analyzing service activities over time.

- Models:

    The microservice is built around several key models:
    - Technician: Represents the service personnel responsible for vehicle maintenance and repair.
    - Appointment: Stores information about scheduled service appointments, including date, time, assigned technician, and customer details.
    - AutomobileVO (Value Object): This model is used to share important details about automobiles.

- Polls Inventory for Automobile Updates:
    - The Service Microservice continuously monitors the inventory for updates related to automobiles.
 
## Sales microservice

- The Sales Microservice is a core componenent of CarCar. This microservice handles customer interactions, and the management of sales and salespeople.
- Models and Funtionality
    - AutomobileVO
        - Represents the unique identity of vehicles in the inventory through their VIN. AutomobileVO tracks the vehicles in inventory as well as their sold status via a poller that requests for data every 60 seconds.
    - Salespeople
        - This system allows for the listing and creation of salespeople using the Salesperson model, keeping track of salespeople's names and IDs.
    - Customers
        - The customers system allows the listing and creation of customers using the Customer model. It tracks the names, addresses, and phone numbers of CarCar customers.
    - Sales
        - This system oversees the processing of sales transactions. Keeping track of a sale's price, while associating every sale with the appropriate salesperson, customer, and automobile.


## Design
Link to excalidraw: https://excalidraw.com/#room=54f99041d071485b1ed7,0Z3Yfy99h_V445qvPR7SIg
![Diagram](https://gitlab.com/jmelendrez5640/project-beta/-/raw/1e19a600e15a424deed09b9fb40a6e882540e65a/carpic.png)


## Project URLs

### INVENTORY URLS

- Manufacturer List: [http://localhost:3000/manufacturers](http://localhost:3000/manufacturers)
- Create a Manufacturer: [http://localhost:3000/manufacturers/create](http://localhost:3000/manufacturers/create)
- Models List: [http://localhost:3000/models](http://localhost:3000/models)
- Create a Model: [http://localhost:3000/models/create](http://localhost:3000/models/create)
- Automobiles List: [http://localhost:3000/automobiles](http://localhost:3000/automobiles)
- Create an Automobile: [http://localhost:3000/automobiles/create](http://localhost:3000/automobiles/create)

### SALES URLS

- Customers List: [http://localhost:3000/customers](http://localhost:3000/customers)
- Create a Customer: [http://localhost:3000/customers/create](http://localhost:3000/customers/create)
- Salespeople List: [http://localhost:3000/salespeople](http://localhost:3000/salespeople)
- Create a Salesperson: [http://localhost:3000/salespeople/create](http://localhost:3000/salespeople/create)
- Sales List: [http://localhost:3000/sales](http://localhost:3000/sales)
- Sales by Salesperson List: [http://localhost:3000/sales/salesperson](http://localhost:3000/sales/salesperson)
- Create a Sale: [http://localhost:3000/sales/create](http://localhost:3000/sales/create)

### SERVICE URLS

- Technicians List: [http://localhost:3000/technicians/](http://localhost:3000/technicians/)
- Create a Technician: [http://localhost:3000/technicians/create](http://localhost:3000/technicians/create)
- Appointments List: [http://localhost:3000/appointments](http://localhost:3000/appointments)
- Create an Appointment: [http://localhost:3000/appointments/create](http://localhost:3000/appointments/create)
- Appointment History: [http://localhost:3000/appointments/history](http://localhost:3000/appointments/history)

## PROJECT PORTS

### MANUFACTURERS

- List manufacturers: GET - [http://localhost:8100/api/manufacturers/](http://localhost:8100/api/manufacturers/)
- Create a manufacturer: POST - [http://localhost:8100/api/manufacturers/](http://localhost:8100/api/manufacturers/)
- Get a specific manufacturer: GET - [http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)
- Update a specific manufacturer: PUT - [http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)
- Delete a specific manufacturer: DELETE - [http://localhost:8100/api/manufacturers/:id/](http://localhost:8100/api/manufacturers/:id/)

### VEHICLES

- List vehicle models: GET - [http://localhost:8100/api/models/](http://localhost:8100/api/models/)
- Create a vehicle model: POST - [http://localhost:8100/api/models/](http://localhost:8100/api/models/)
- Get a specific vehicle model: GET - [http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)
- Update a specific vehicle model: PUT - [http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)
- Delete a specific vehicle model: DELETE - [http://localhost:8100/api/models/:id/](http://localhost:8100/api/models/:id/)

### AUTOMOBILES

- List automobiles: GET - [http://localhost:8100/api/automobiles/](http://localhost:8100/api/automobiles/)
- Create an automobile: POST - [http://localhost:8100/api/automobiles/](http://localhost:8100/api/automobiles/)
- Get a specific automobile: GET - [http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)
- Update a specific automobile: PUT - [http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)
- Delete a specific automobile: DELETE - [http://localhost:8100/api/automobiles/:vin/](http://localhost:8100/api/automobiles/:vin/)

### TECHNICIANS

- List technicians: GET - [http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)
- Create a technician: POST - [http://localhost:8080/api/technicians/](http://localhost:8080/api/technicians/)
- Delete a specific technician: DELETE - [http://localhost:8080/api/technicians/:id/](http://localhost:8080/api/technicians/:id/)

### APPOINTMENTS

- List appointments: GET - [http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)
- Create an appointment: POST - [http://localhost:8080/api/appointments/](http://localhost:8080/api/appointments/)
- Delete an appointment: DELETE - [http://localhost:8080/api/appointments/:id/](http://localhost:8080/api/appointments/:id/)
- Set appointment status to "canceled": PUT - [http://localhost:8080/api/appointments/:id/cancel/](http://localhost:8080/api/appointments/:id/cancel/)
- Set appointment status to "finished": PUT - [http://localhost:8080/api/appointments/:id/finish/](http://localhost:8080/api/appointments/:id/finish/)

### SALESPEOPLE

- List salespeople: GET - [http://localhost:8090/api/salespeople/](http://localhost:8090/api/salespeople/)
- Create a salesperson: POST - [http://localhost:8090/api/salespeople/](http://localhost:8090/api/salespeople/)
- Delete a specific salesperson: DELETE - [http://localhost:8090/api/salespeople/:id/](http://localhost:8090/api/salespeople/:id/)

### CUSTOMERS

- List customers: GET - [http://localhost:8090/api/customers/](http://localhost:8090/api/customers/)
- Create a customer: POST - [http://localhost:8090/api/customers/](http://localhost:8090/api/customers/)
- Delete a specific customer: DELETE - [http://localhost:8090/api/customers/:id/](http://localhost:8090/api/customers/:id/)

### SALES

- List sales: GET - [http://localhost:8090/api/sales/](http://localhost:8090/api/sales/)
- Create a sale: POST - [http://localhost:8090/api/sales/](http://localhost:8090/api/sales/)
- Delete a sale: DELETE - [http://localhost:8090/api/sales/:id/](http://localhost:8090/api/sales/:id/)


### Sales API

- Be sure an Automobile, Salesperson, and Customer has been created before creating a sale

#### Request examples for Sales API

- Create a Salesperson, employee id must be unique

    - Request:
    
    ```json
    {
        "first_name": "John",
        "last_name": "Smith",
        "employee_id": "10"
    }
    ```
    
    - Expected Return (status 201 Created):
    
    ```json
    {
        "first_name": "John",
        "last_name": "Smith",
        "employee_id": "10",
        "id": 1
    }
    ```

- Create a Customer, phone number must be unique

    - Request:
    
    ```json
    {
        "first_name": "Jim",
        "last_name": "Doe",
        "address": "12422 Test Ave, Sacramento, CA",
        "phone_number": "243-124-1413"
    }
    ```
    
    - Expected Return (status 201 Created):
    
    ```json
    {
        "first_name": "Jim",
        "last_name": "Doe",
        "address": "12422 Test Ave, Sacramento, CA",
        "phone_number": "243-124-1413",
        "id": 1
    }
    ```

- Create a Sale, be sure to use an available Automobile VIN, Salesperson id, and Customer id

    - Request:
    
    ```json
    {
        "price": "$32,000",
        "automobile": "1DSHE57Y96U133571",
        "sales_person": "1",
        "customer": "1"
    }
    ```
    
    - Expected Return (status 201 Created):
    
    ```json
    {
        "price": "$32,000",
        "automobile": {
            "vin": "1DSHE57Y96U133571",
            "sold": true
        },
        "sales_person": {
            "first_name": "John",
            "last_name": "Smith",
            "employee_id": 10,
            "id": 1
        },
        "customer": {
            "first_name": "Jim",
            "last_name": "Doe",
            "address": "12422 Test Ave, Sacramento, CA",
            "phone_number": "243-124-1413",
            "id": 1
        },
        "id": 1
    }
    ```

- Automobile sold status is updated via the front-end, after a sale is made

- List Salespeople

    - Expected Return:
    
    ```json
    {
        "salespeople": [
            {
                "first_name": "John",
                "last_name": "Smith",
                "employee_id": 10,
                "id": 1
            }
        ]
    }
    ```

- List Customers

    - Expected Return:
    
    ```json
    {
        "customers": [
            {
                "first_name": "Jim",
                "last_name": "Doe",
                "address": "12422 Test Ave, Sacramento, CA",
                "phone_number": "243-124-1413",
                "id": 1
            }
        ]
    }
    ```

- **List Sales**

    - **Expected Return:**
    
    ```json
    {
        "sales": [
            {
                "price": "$32,000",
                "automobile": {
                    "vin": "1DSHE57Y96U133571",
                    "sold": true
                },
                "sales_person": {
                    "first_name": "John",
                    "last_name": "Smith",
                    "employee_id": 1,
                    "id": 10
                },
                "customer": {
                    "first_name": "Jim",
                    "last_name": "Doe",
                    "address": "12422 Test Ave, Sacramento, CA",
                    "phone_number": "243-124-1413",
                    "id": 1
                },
                "id": 1
            }
        ]
    }
    ```



## Value Objects
 - Sales API
    - AutomobileVO
        - The AutomobileVO is a value object that represents the automobiles in the inventory API. It maintains a view of an external automobile inventory by updating every 60 seconds via a poller


## Service API

Request examples for Service API

- Create appoitment (Technician id must exist)

    - Request:

    ```json
    {
    "date_time": "2023-9-2 10:00",
    "reason": "Vehicle maintenance",
    "status": "Pending",
    "vin": "HFNAUEF85864",  
    "customer": "Bob Burger",
    "vip": false,
    "technician": 11
	
    }
    ```

- Expexted return 
    ```json
    {
	"date_time": "2023-9-2 10:00",
	"time": null,
	"reason": "Vehicle maintenance",
	"status": "Pending",
	"vin": "HFNAUEF85864",
	"customer": "Bob Burger",
	"id": 13,
	"vip": false,
	"technician": {
		"first_name": "noah",
		"last_name": "naigzy",
		"employee_id": "1",
		"id": 11
	}
    }
    ```

- Create Technician (id must not be used by any current techs)

    - Request:

    ```json
    {
    "first_name": "Joey",
    "last_name": "Diaz",
    "employee_id": "1"
    }
    ```

- Expected return
    ```json
    {
	"first_name": "Joey",
	"last_name": "Diaz",
	"employee_id": "1",
	"id": 10
    }
    ```


- List Technicians
```json
{
    "technicians": [
        {
            "first_name": "noah",
            "last_name": "naigzy",
            "employee_id": "1",
            "id": 11
        },
        {
            "first_name": "dead",
            "last_name": "pool",
            "employee_id": "2",
            "id": 12
        },
        {
            "first_name": "john smith",
            "last_name": "smith",
            "employee_id": "3",
            "id": 13
        },
        {
            "first_name": "jake",
            "last_name": "smith",
            "employee_id": "4",
            "id": 14
        },
        {
            "first_name": "gandolf ",
            "last_name": "jr",
            "employee_id": "11",
            "id": 15
        },
        {
            "first_name": "Clair",
            "last_name": "sin",
            "employee_id": "12",
            "id": 16
        }
    ]
}
```


- List Appointments
```json
{
    "appointments": [
        {
            "date_time": "2023-11-11T09:00:00+00:00",
            "time": null,
            "reason": "Vehicle maintenance",
            "status": "Pending",
            "vin": "ASNGFUW1231124",
            "customer": "Ryan Marks",
            "id": 2,
            "vip": false,
            "technician": {
                "first_name": "Tin",
                "last_name": "Tin",
                "employee_id": "TECH3",
                "id": 3
            }
        },
        {
            "date_time": "2023-09-02T11:00:00+00:00",
            "time": null,
            "reason": "Vehicle maintenance",
            "status": "Pending",
            "vin": "ASNGFUDGS7631124",
            "customer": "Jake Traviskin",
            "id": 3,
            "vip": false,
            "technician": {
                "first_name": "Tilly",
                "last_name": "Wacker",
                "employee_id": "TECH2",
                "id": 2
            }
        },
        {
            "date_time": "2023-10-25T14:30:00+00:00",
            "time": null,
            "reason": "Vehicle maintenance",
            "status": "finished",
            "vin": "ABC123456789",
            "customer": "John Doe",
            "id": 1,
            "vip": false,
            "technician": {
                "first_name": "John",
                "last_name": "Doe",
                "employee_id": "TECH1",
                "id": 1
            }
        },
        {
            "date_time": "2023-09-02T11:00:00+00:00",
            "time": null,
            "reason": "Vehicle maintenance",
            "status": "Pending",
            "vin": "ASNGFUDGS7631124",
            "customer": "Jake Traviskin",
            "id": 4,
            "vip": false,
            "technician": {
                "first_name": "Tilly",
                "last_name": "Wacker",
                "employee_id": "TECH2",
                "id": 2
            }
        },
        {
            "date_time": "2023-09-02T10:00:00+00:00",
            "time": null,
            "reason": "Vehicle maintenance",
            "status": "Pending",
            "vin": "HFNAUEF85864",
            "customer": "Bob Burger",
            "id": 5,
            "vip": false,
            "technician": {
                "first_name": "Tilly",
                "last_name": "Wacker",
                "employee_id": "TECH2",
                "id": 2
            }
        }
    ]
}
```
  